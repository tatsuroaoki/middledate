let button = document.getElementById("button")
button.addEventListener("click",middleDate)
let startDate = new Date('January 1, 2019')
let endDate = new Date('April 1, 2019')

function middleDate() {
	let msToDay = 1000 * 60 * 60 * 24
	let msInBetween = (endDate.getTime() - startDate.getTime()) 
	let halfDifference = msInBetween / 2
	let middleDate = new Date(startDate.getTime() + halfDifference)

	let answer = document.getElementById("answer");
	answer.innerHTML = middleDate

}